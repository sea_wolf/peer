# PEER

Remind teams of recent GitHub Pull Requests (PRs) by posting a message to Slack.

![Screenshot of Slack message](https://bitbucket.org/repo/r9XBGKa/images/3416539287-image.png)

### Overview

The application:

* loads [configuration](#configuration) from the environment
* querying GitHub for pull requests that are:
  * not in Draft state
  * assigned to the team for review
  * created between 3 and 6 hours ago
* posting a message to Slack

### Configuration

##### Slack

* `SLACK_AUTH_TOKEN` is set-up according to the Slack documentation
* `SLACK_CHANNEL` is the name of the channel to which the message is posted
* `SLACK_DEBUG` can be set to show output while the program is running

##### GitHub

* `GITHUB_AUTH_TOKEN` is set-up according to the GitHub documentation
* `GITHUB_ORG` is the name of the GitHub organisation (singular) to which the repositories and team belongs
* `GITHUB_REPOS` is the name of the repositories (multiple; comma-separated) in which Pull Requests are to be found
* `GITHUB_TEAM` is the name of the team (singular) to which the Pull Requests are assigned for review
* `GITHUB_DEBUG` can be set to show output while the program is running

### Name & Aim

PEER is an acronym for it's aim: _Problems Examined, Exchanged, Resolved._
