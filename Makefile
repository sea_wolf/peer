
IMAGE_NAME=peer
CONTAINER_NAME=peer

.PHONY: all dev run shell stop clean

all: clean build run

dev: build run

build: Dockerfile
	docker build -t ${IMAGE_NAME}:latest .
	[ -r .env ] || cp .env.example .env

run:
	docker run -it --rm --name ${CONTAINER_NAME} --env-file ./.env ${IMAGE_NAME}:latest /peer

# shell:
# 	docker exec -it ${CONTAINER_NAME} /bin/sh

# stop:
# 	docker container stop ${CONTAINER_NAME}

clean:
	docker ps -q --filter name=${CONTAINER_NAME} | grep -q . && docker container stop ${CONTAINER_NAME} || true
	docker image ls -q ${IMAGE_NAME} | grep -q . && docker image rm ${IMAGE_NAME} || true