package slack

import (
	"log"
	"testing"

	"github.com/google/go-github/v33/github"
)

func TestNew(t *testing.T) {
	tests := []struct {
		name                  string
		input                 *github.PullRequest
		expectedURL           string
		expectedRepoName      string
		expectedTitle         string
		expectedDescription   string
		expectedUserName      string
		expectedUserAvatarURL string
	}{
		{
			name:                  "no info",
			expectedURL:           "",
			expectedRepoName:      "",
			expectedTitle:         "",
			expectedDescription:   "",
			expectedUserName:      "",
			expectedUserAvatarURL: "",
			input: &github.PullRequest{
				HTMLURL: github.String(""),
				Title:   github.String(""),
				Body:    github.String(""),
				User: &github.User{
					Login:     github.String(""),
					AvatarURL: github.String(""),
				},
				Base: &github.PullRequestBranch{
					Repo: &github.Repository{
						FullName: github.String(""),
					},
				},
			},
		},
		{
			name:                  "personal PR",
			expectedURL:           "https://github.com/john.doe/my-repo/pull/123",
			expectedRepoName:      "john.doe/my-repo",
			expectedTitle:         "Fix a Thing",
			expectedDescription:   "This PR fixes the `Thing`",
			expectedUserName:      "john.doe",
			expectedUserAvatarURL: "https://avatars.com/?name=john.doe",
			input: &github.PullRequest{
				HTMLURL: github.String("https://github.com/john.doe/my-repo/pull/123"),
				Title:   github.String("Fix a Thing"),
				Body:    github.String("This PR fixes the `Thing`"),
				User: &github.User{
					Login:     github.String("john.doe"),
					AvatarURL: github.String("https://avatars.com/?name=john.doe"),
				},
				Base: &github.PullRequestBranch{
					Repo: &github.Repository{
						FullName: github.String("john.doe/my-repo"),
					},
				},
			},
		},
		{
			name:                  "organisation PR",
			expectedURL:           "https://github.com/my-org/my-repo",
			expectedRepoName:      "my-org/my-repo",
			expectedTitle:         "BUG-123 - fix the Thing",
			expectedDescription:   "There's a bug in `Thing`, see ticket for details!",
			expectedUserName:      "john.doe",
			expectedUserAvatarURL: "https://avatars.com/?name=john.doe",
			input: &github.PullRequest{
				HTMLURL: github.String("https://github.com/my-org/my-repo"),
				Title:   github.String("BUG-123 - fix the Thing"),
				Body:    github.String("There's a bug in `Thing`, see ticket for details!"),
				User: &github.User{
					Login:     github.String("john.doe"),
					AvatarURL: github.String("https://avatars.com/?name=john.doe"),
				},
				Base: &github.PullRequestBranch{
					Repo: &github.Repository{
						FullName: github.String("my-org/my-repo"),
					},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := New(test.input)

			if actual.URL != test.expectedURL {
				log.Fatalf("New() %v failed; expected URL to be %v but got %v", test.name, test.expectedURL, actual.URL)
			}

			if actual.RepoName != test.expectedRepoName {
				log.Fatalf("New() %v failed; expected RepoName to be %v but got %v", test.name, test.expectedRepoName, actual.RepoName)
			}

			if actual.Title != test.expectedTitle {
				log.Fatalf("New() %v failed; expected Title to be %v but got %v", test.name, test.expectedTitle, actual.Title)
			}

			if actual.Description != test.expectedDescription {
				log.Fatalf("New() %v failed; expected Description to be %v but got %v", test.name, test.expectedDescription, actual.Description)
			}

			if actual.UserName != test.expectedUserName {
				log.Fatalf("New() %v failed; expected UserName to be %v but got %v", test.name, test.expectedUserName, actual.UserName)
			}

			if actual.UserAvatarURL != test.expectedUserAvatarURL {
				log.Fatalf("New() %v failed; expected UserAvatarURL to be %v but got %v", test.name, test.expectedUserAvatarURL, actual.UserAvatarURL)
			}
		})
	}
}

func TestRepoURL(t *testing.T) {
	tests := []struct {
		name     string
		input    PullRequest
		expected string
	}{
		{
			name:     "empty",
			expected: "",
			input: PullRequest{
				RepoName: "",
			},
		},
		{
			name:     "organisation",
			expected: "https://github.com/my-org/my-repo",
			input: PullRequest{
				RepoName: "my-org/my-repo",
			},
		},
		{
			name:     "personal",
			expected: "https://github.com/john.doe/my-repo",
			input: PullRequest{
				RepoName: "john.doe/my-repo",
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := test.input.repoURL()

			if actual != test.expected {
				log.Fatalf("New() %v failed; expected repoURL to be %v but got %v", test.name, test.expected, actual)
			}
		})
	}
}

func TestAuthorURL(t *testing.T) {
	tests := []struct {
		name     string
		input    PullRequest
		expected string
	}{
		{
			name:     "empty",
			expected: "",
			input: PullRequest{
				UserName: "",
			},
		},
		{
			name:     "personal",
			expected: "https://github.com/john.doe",
			input: PullRequest{
				UserName: "john.doe",
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := test.input.authorURL()

			if actual != test.expected {
				log.Fatalf("New() %v failed; expected authorURL to be %v but got %v", test.name, test.expected, actual)
			}
		})
	}
}
