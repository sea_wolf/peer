package slack

import (
	"bitbucket.org/sea_wolf/peer/internal/config"
	"github.com/slack-go/slack"
)

// API connects to the real thing
type API interface {
	AuthTest() (*slack.AuthTestResponse, error)
	PostMessage(channelID string, options ...slack.MsgOption) (string, string, error)
}

var (
	// Config brings in our configuration
	Config config.Config

	// Slack connects to the Slack API for us
	Slack API
)

// Init is to be called before anything
func Init() {
	Config = config.Init()

	Slack = slack.New(Config.Slack.AuthToken, slack.OptionDebug(Config.Slack.Debug))
}
