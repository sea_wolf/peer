package slack

import (
	"fmt"

	"github.com/google/go-github/v33/github"
)

// PullRequest is an adapter for Github PRs
type PullRequest struct {
	URL           string
	RepoName      string
	Title         string
	Description   string
	UserName      string
	UserAvatarURL string
}

// New converts Github PRs into Slack message data
func New(p *github.PullRequest) PullRequest {
	return PullRequest{
		URL:           *p.HTMLURL,
		RepoName:      *p.Base.Repo.FullName,
		Title:         *p.Title,
		Description:   *p.Body,
		UserName:      *p.User.Login,
		UserAvatarURL: *p.User.AvatarURL,
	}
}

func (p PullRequest) repoURL() string {
	if len(p.RepoName) == 0 {
		return ""
	}

	return fmt.Sprintf("https://github.com/%v", p.RepoName)
}

func (p PullRequest) authorURL() string {
	if len(p.UserName) == 0 {
		return ""
	}

	return fmt.Sprintf("https://github.com/%v", p.UserName)
}
