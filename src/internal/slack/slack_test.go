package slack

import (
	"fmt"
	"time"

	"github.com/slack-go/slack"
)

type TestSlackAPI struct {
	AuthTestResponse *slack.AuthTestResponse
	AuthError        error
	PostMessageError error
}

func (s TestSlackAPI) AuthTest() (*slack.AuthTestResponse, error) {
	return s.AuthTestResponse, s.AuthError
}

func (s TestSlackAPI) PostMessage(channelID string, options ...slack.MsgOption) (string, string, error) {
	if s.PostMessageError != nil {
		return "", "", s.PostMessageError
	}

	return "CHANNEL_ID", fmt.Sprintf("%d", time.Now().Unix()), nil
}
