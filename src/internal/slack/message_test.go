package slack

import (
	"errors"
	"log"
	"testing"
)

func TestSend(t *testing.T) {
	tests := []struct {
		name     string
		input    Message
		slack    API
		expected error
	}{
		{
			name: "populated",
			input: Message{PullRequests: []PullRequest{
				{
					URL:           "https://github.com/...",
					RepoName:      "my-cool-code",
					Title:         "Super Sweet Feature",
					Description:   "Adds some great stuff",
					UserName:      "john.doe",
					UserAvatarURL: "https://avatars.github.com/john.doe",
				},
			}},
			slack:    TestSlackAPI{},
			expected: nil,
		},
		{
			name:     "empty",
			input:    Message{PullRequests: []PullRequest{}},
			slack:    TestSlackAPI{},
			expected: NoPullRequests{},
		},
		{
			name: "unsuccessful",
			input: Message{PullRequests: []PullRequest{
				{
					URL:           "https://github.com/...",
					RepoName:      "my-cool-code",
					Title:         "Super Sweet Feature",
					Description:   "Adds some great stuff",
					UserName:      "john.doe",
					UserAvatarURL: "https://avatars.github.com/john.doe",
				},
			}},
			slack:    TestSlackAPI{PostMessageError: errors.New("Bang")},
			expected: errors.New("Bang"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			Slack = test.slack
			_, _, actual := test.input.Send()

			if actual != nil || test.expected != nil {
				if actual.Error() != test.expected.Error() {
					log.Fatalf("Send() %v failed; expected error to be %v but got %v", test.name, test.expected, actual)
				}
			} else {
				if actual != test.expected {
					log.Fatalf("Send() %v failed; expected error to be %v but got %v", test.name, test.expected, actual)
				}
			}
		})
	}
}

func TestBlocks(t *testing.T) {
	tests := []struct {
		name     string
		input    Message
		expected int
	}{
		{
			name:     "none",
			input:    Message{},
			expected: 1,
		},
		{
			name: "empty",
			input: Message{
				PullRequests: []PullRequest{},
			},
			expected: 1,
		},
		{
			name: "one",
			input: Message{
				PullRequests: []PullRequest{
					{},
				},
			},
			expected: 3,
		},
		{
			name: "three",
			input: Message{
				PullRequests: []PullRequest{
					{},
					{},
					{},
				},
			},
			expected: 7,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := len(test.input.blocks())

			if actual != test.expected {
				log.Fatalf("blocks() %v failed; expected length to be %v but got %v", test.name, test.expected, actual)
			}
		})
	}
}
func TestMessageBlock(t *testing.T) {
	message := Message{}

	tests := []struct {
		name  string
		input PullRequest
	}{
		{
			name: "empty",
		},
		{
			name:  "empty PR",
			input: PullRequest{},
		},
		{
			name: "personal PR",
			input: PullRequest{
				URL:           "https://github.com/john.doe/my-repo/pull/123",
				RepoName:      "john.doe/my-repo",
				Title:         "Fix a Thing",
				Description:   "Fixes a `Thing` because it was broken.",
				UserName:      "john.doe",
				UserAvatarURL: "https://avatars.com/?name=john.doe",
			},
		},
		{
			name: "organisation PR",
			input: PullRequest{
				URL:           "https://github.com/my-org/my-repo/pull/123",
				RepoName:      "my-org/my-repo",
				Title:         "Fix a Thing",
				Description:   "Fixes a `Thing` because it was broken.",
				UserName:      "john.doe",
				UserAvatarURL: "https://avatars.com/?name=john.doe",
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			// just making sure it runs, not testing 3rd-party code
			message.messageBlock(test.input)
		})
	}
}
func TestPullRequests(t *testing.T) {
	tests := []struct {
		name       string
		inputLimit float64
		input      Message
		expected   int
	}{
		{
			name:       "none with zero limit",
			input:      Message{},
			inputLimit: 0,
			expected:   0,
		},
		{
			name: "none with zero limit",
			input: Message{
				PullRequests: []PullRequest{},
			},
			inputLimit: 0,
			expected:   0,
		},
		{
			name:       "under limit",
			inputLimit: 2,
			input: Message{
				PullRequests: []PullRequest{
					{
						URL:           "https://github.com/john.doe/my-repo/pull/123",
						RepoName:      "john.doe/my-repo",
						Title:         "Fix a Thing",
						Description:   "Fixes a `Thing` because it was broken.",
						UserName:      "john.doe",
						UserAvatarURL: "https://avatars.com/?name=john.doe",
					},
					{
						URL:           "https://github.com/john.doe/my-repo/pull/123",
						RepoName:      "john.doe/my-repo",
						Title:         "Fix a Thing",
						Description:   "Fixes a `Thing` because it was broken.",
						UserName:      "john.doe",
						UserAvatarURL: "https://avatars.com/?name=john.doe",
					},
					{
						URL:           "https://github.com/john.doe/my-repo/pull/123",
						RepoName:      "john.doe/my-repo",
						Title:         "Fix a Thing",
						Description:   "Fixes a `Thing` because it was broken.",
						UserName:      "john.doe",
						UserAvatarURL: "https://avatars.com/?name=john.doe",
					},
					{
						URL:           "https://github.com/john.doe/my-repo/pull/123",
						RepoName:      "john.doe/my-repo",
						Title:         "Fix a Thing",
						Description:   "Fixes a `Thing` because it was broken.",
						UserName:      "john.doe",
						UserAvatarURL: "https://avatars.com/?name=john.doe",
					},
				},
			},
			expected: 2,
		},
		{
			name:       "matching limit",
			inputLimit: 2,
			input: Message{
				PullRequests: []PullRequest{
					{
						URL:           "https://github.com/john.doe/my-repo/pull/123",
						RepoName:      "john.doe/my-repo",
						Title:         "Fix a Thing",
						Description:   "Fixes a `Thing` because it was broken.",
						UserName:      "john.doe",
						UserAvatarURL: "https://avatars.com/?name=john.doe",
					},
					{
						URL:           "https://github.com/john.doe/my-repo/pull/123",
						RepoName:      "john.doe/my-repo",
						Title:         "Fix a Thing",
						Description:   "Fixes a `Thing` because it was broken.",
						UserName:      "john.doe",
						UserAvatarURL: "https://avatars.com/?name=john.doe",
					},
				},
			},
			expected: 2,
		},
		{
			name:       "over limit",
			inputLimit: 4,
			input: Message{
				PullRequests: []PullRequest{
					{
						URL:           "https://github.com/john.doe/my-repo/pull/123",
						RepoName:      "john.doe/my-repo",
						Title:         "Fix a Thing",
						Description:   "Fixes a `Thing` because it was broken.",
						UserName:      "john.doe",
						UserAvatarURL: "https://avatars.com/?name=john.doe",
					},
					{
						URL:           "https://github.com/john.doe/my-repo/pull/123",
						RepoName:      "john.doe/my-repo",
						Title:         "Fix a Thing",
						Description:   "Fixes a `Thing` because it was broken.",
						UserName:      "john.doe",
						UserAvatarURL: "https://avatars.com/?name=john.doe",
					},
				},
			},
			expected: 2,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := len(test.input.pullRequests(test.inputLimit))

			if actual != test.expected {
				log.Fatalf("pullRequests() %v failed; expected length to be %v but got %v", test.name, test.expected, actual)
			}
		})
	}
}

func TestPRMessageText(t *testing.T) {
	message := Message{}

	tests := []struct {
		name     string
		input    PullRequest
		expected string
	}{
		{
			name:     "none",
			expected: "<|**>" + "\n" + "_in <|> by <|>_" + "\n" + "> ",
		},
		{
			name:     "empty",
			input:    PullRequest{},
			expected: "<|**>" + "\n" + "_in <|> by <|>_" + "\n" + "> ",
		},
		{
			name: "personal PR",
			input: PullRequest{
				URL:           "https://github.com/john.doe/my-repo/pull/123",
				RepoName:      "john.doe/my-repo",
				Title:         "Fix a Thing",
				Description:   "Fixes a `Thing` because it was broken.",
				UserName:      "john.doe",
				UserAvatarURL: "https://avatars.com/?name=john.doe",
			},
			expected: "<https://github.com/john.doe/my-repo/pull/123|*Fix a Thing*>" + "\n" +
				"_in <https://github.com/john.doe/my-repo|john.doe/my-repo> by <https://github.com/john.doe|john.doe>_" + "\n" +
				"> Fixes a `Thing` because it was broken.",
		},
		{
			name: "organisation PR",
			input: PullRequest{
				URL:           "https://github.com/my-org/my-repo/pull/123",
				RepoName:      "my-org/my-repo",
				Title:         "Fix a Thing",
				Description:   "Fixes a `Thing` because it was broken.",
				UserName:      "john.doe",
				UserAvatarURL: "https://avatars.com/?name=john.doe",
			},
			expected: "<https://github.com/my-org/my-repo/pull/123|*Fix a Thing*>" + "\n" +
				"_in <https://github.com/my-org/my-repo|my-org/my-repo> by <https://github.com/john.doe|john.doe>_" + "\n" +
				"> Fixes a `Thing` because it was broken.",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := message.prMessageText(test.input)

			if actual != test.expected {
				log.Fatalf("prMessageText(PullRequest) %v failed; expected output to be %v but got %v", test.name, test.input, actual)
			}
		})
	}
}

func TestIndexOfFirstLineInString(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		expected int
	}{
		{
			name:     "none",
			expected: 0,
		},
		{
			name:     "empty",
			input:    "",
			expected: 0,
		},
		{
			name:     "none",
			input:    " abc def ghi ",
			expected: 13,
		},
		{
			name:     "contains",
			input:    "abc de\nf ghi",
			expected: 6,
		},
		{
			name:     "multiple",
			input:    "abc\ndef\nghi",
			expected: 3,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := indexOfFirstLineInString(test.input)

			if actual != test.expected {
				log.Fatalf("indexOfFirstLineInString() %v failed; expected output to be %v but got %v", test.name, test.input, actual)
			}
		})
	}
}

func TestTruncateString(t *testing.T) {
	tests := []struct {
		name        string
		inputString string
		inputLength int
		expected    string
	}{
		{
			name:     "nothing",
			expected: "",
		},
		{
			name:        "wanting empty",
			inputString: "1234567890",
			inputLength: 0,
			expected:    "",
		},
		{
			name:        "empty",
			inputString: "",
			inputLength: 99,
			expected:    "",
		},
		{
			name:        "short",
			inputString: " abc def ghi ",
			inputLength: 99,
			expected:    " abc def ghi ",
		},
		{
			name:        "equal",
			inputString: "abcdefghi",
			inputLength: 9,
			expected:    "abcdefghi",
		},
		{
			name:        "long",
			inputString: " abc def ghi ",
			inputLength: 9,
			expected:    " abc def ",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := truncateString(test.inputString, test.inputLength)

			if actual != test.expected {
				log.Fatalf("truncateString() %v failed; expected output to be %v but got %v", test.name, test.inputString, actual)
			}
		})
	}
}
