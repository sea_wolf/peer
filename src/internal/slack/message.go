package slack

import (
	"fmt"
	"math"
	"strings"

	"github.com/slack-go/slack"
)

const messageDescriptionMaxLength = float64(250)
const maxPullRequestCountPerMessage = float64(10)

// Message converts a list of Github PRs to a Slack message
type Message struct {
	PullRequests []PullRequest
}

type NoPullRequests struct{}

func (NoPullRequests) Error() string {
	return "no PRs found"
}

// Send pushes the message to the configured channel
func (m Message) Send() (string, string, error) {
	if m.isEmpty() {
		return "", "", new(NoPullRequests)
	}

	channelID, timestamp, error := Slack.PostMessage(
		Config.Slack.Channel,
		slack.MsgOptionBlocks(m.blocks()...),
	)

	return channelID, timestamp, error
}

func (m Message) isEmpty() bool {
	return len(m.PullRequests) == 0
}

func (m Message) blocks() []slack.Block {
	list := []slack.Block{}

	list = append(list,
		slack.SectionBlock{
			Type: "section",
			Text: &slack.TextBlockObject{
				Type: slack.MarkdownType,
				Text: "We have some *Pull Requests* to review!",
			},
		},
	)

	for _, pr := range m.pullRequests(maxPullRequestCountPerMessage) {
		list = append(list,
			slack.SectionBlock{
				Type: "divider",
			},

			m.messageBlock(pr),
		)
	}

	return list
}

func (m Message) pullRequests(limit float64) []PullRequest {
	maxPullRequestCount := math.Min(limit, float64(len(m.PullRequests)))

	return m.PullRequests[0:int(maxPullRequestCount)]
}

func (m Message) messageBlock(p PullRequest) slack.Block {
	return slack.SectionBlock{
		Type: "section",
		Text: &slack.TextBlockObject{
			Type: slack.MarkdownType,
			Text: m.prMessageText(p),
		},
		Accessory: &slack.Accessory{
			ImageElement: &slack.ImageBlockElement{
				Type:     "image",
				ImageURL: p.UserAvatarURL,
				AltText:  p.authorURL(),
			},
		},
	}
}

func (m Message) prMessageText(p PullRequest) string {
	return strings.Join([]string{
		fmt.Sprintf(
			"<%v|*%v*>",
			p.URL, p.Title,
		),

		fmt.Sprintf(
			"_in %v by %v_",
			fmt.Sprintf(
				"<%v|%v>",
				p.repoURL(), p.RepoName,
			),

			fmt.Sprintf(
				"<%v|%v>",
				p.authorURL(), p.UserName,
			),
		),

		fmt.Sprintf(
			"> %v",

			truncateString(
				truncateString(
					p.Description,
					indexOfFirstLineInString(p.Description),
				),
				int(messageDescriptionMaxLength),
			),
		),
	}, "\n")
}

func indexOfFirstLineInString(str string) int {
	idx := strings.Index(str, "\n")

	if idx == -1 {
		return len(str)
	}

	return idx
}

func truncateString(str string, length int) string {
	if length <= 0 {
		return ""
	}

	truncated := ""
	truncatedLength := 0
	for _, char := range str {
		truncated += string(char)
		truncatedLength++

		if truncatedLength >= length {
			break
		}
	}

	return truncated
}
