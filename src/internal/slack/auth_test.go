package slack

import (
	"errors"
	"io/ioutil"
	"log"
	"os"
	"testing"

	"github.com/slack-go/slack"
)

func TestInit(t *testing.T) {
	t.Run("sets the Config and Github variables from required data", func(t *testing.T) {
		os.Setenv("SLACK_AUTH_TOKEN", "-")
		os.Setenv("SLACK_CHANNEL", "cool-dev-team")
		os.Setenv("GITHUB_AUTH_TOKEN", "-")
		os.Setenv("GITHUB_ORG", "acme-ltd")
		os.Setenv("GITHUB_REPOS", "thing,stuff")
		os.Setenv("GITHUB_TEAM", "devs")

		Init()
	})
}

func TestAuth(t *testing.T) {
	log.SetOutput(ioutil.Discard)

	tests := []struct {
		name     string
		slack    TestSlackAPI
		expected bool
	}{
		{
			name:     "When Slack doesn't like us",
			slack:    TestSlackAPI{AuthError: errors.New("Sorry")},
			expected: false,
		},
		{
			name: "When Slack likes us",
			slack: TestSlackAPI{AuthTestResponse: &slack.AuthTestResponse{
				URL:  "https://some.slack.com",
				Team: "AwesomeSaucers",
				User: "@john.doe",
			}},
			expected: true,
		},
	}

	for _, tt := range tests {
		Slack = tt.slack

		t.Run(tt.name, func(t *testing.T) {
			if returned := Auth(); returned != tt.expected {
				t.Errorf("Auth() returned %v but expected %v", returned, tt.expected)
			}
		})
	}
}
