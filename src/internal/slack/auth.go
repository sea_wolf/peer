package slack

import (
	"log"
)

// Auth is an example
func Auth() bool {
	auth, err := Slack.AuthTest()
	if err != nil {
		log.Printf("Slack/Auth => token: %s, failure: %s\n", Config.Slack.AuthToken, err)
		return false
	}

	if Config.Slack.Debug {
		log.Printf("Slack/Auth => URL: %s · Team: %s · User: %s", auth.URL, auth.Team, auth.User)
	}

	return true
}
