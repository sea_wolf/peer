package github

import (
	"context"
	"errors"
	"log"
	"math/rand"
	"reflect"
	"testing"
	"time"

	"github.com/google/go-github/v33/github"
)

type TestPullRequestFetcher struct {
	Error error
}

func (f TestPullRequestFetcher) List(_ context.Context, _ string, _ string, _ *github.PullRequestListOptions) (_ []*github.PullRequest, _ *github.Response, _ error) {
	if f.Error != nil {
		return nil, nil, f.Error
	}

	pulls := []*github.PullRequest{
		{
			Number:  github.Int(123),
			Draft:   github.Bool(false),
			Title:   github.String("My Cool PR"),
			HTMLURL: github.String("https://github.com/myorg/myapp/pulls/123"),
			Base: &github.PullRequestBranch{
				Repo: &github.Repository{
					Name: github.String("myapp"),
				},
			},
			User: &github.User{
				Login: github.String("1337dev"),
			},
			RequestedTeams: []*github.Team{
				{Slug: github.String(Config.Github.Team)},
			},
			CreatedAt: &[]time.Time{
				time.Now().UTC().Add(-4 * time.Hour),
			}[0],
			UpdatedAt: &[]time.Time{
				time.Now().UTC().Add(-4 * time.Hour),
			}[0],
		},
		{
			Number:  github.Int(456),
			Draft:   github.Bool(true),
			Title:   github.String("My Awesome Thing"),
			HTMLURL: github.String("https://github.com/myorg/myapp/pulls/456"),
			Base: &github.PullRequestBranch{
				Repo: &github.Repository{
					Name: github.String("myapp"),
				},
			},
			User: &github.User{
				Login: github.String("1337dev"),
			},
			RequestedTeams: []*github.Team{
				{Slug: github.String(Config.Github.Team)},
			},
			CreatedAt: &[]time.Time{
				time.Now().UTC().Add(-4 * time.Hour),
			}[0],
			UpdatedAt: &[]time.Time{
				time.Now().UTC().Add(-4 * time.Hour),
			}[0],
		},
		{
			Number:  github.Int(789),
			Draft:   github.Bool(false),
			Title:   github.String("Fix Thing"),
			HTMLURL: github.String("https://github.com/myorg/myapp/pulls/789"),
			Base: &github.PullRequestBranch{
				Repo: &github.Repository{
					Name: github.String("myapp"),
				},
			},
			User: &github.User{
				Login: github.String("1337dev"),
			},
			RequestedTeams: []*github.Team{
				{Slug: github.String(Config.Github.Team)},
			},
			CreatedAt: &[]time.Time{
				time.Now().UTC().Add(-5 * time.Hour),
			}[0],
			UpdatedAt: &[]time.Time{
				time.Now().UTC().Add(-5 * time.Hour),
			}[0],
		},
	}
	return pulls, nil, nil
}

func TestPulls(t *testing.T) {
	Config.Github.Repos = "myapp"
	Config.Github.Team = "devs"

	tests := []struct {
		name       string
		pullCount  int
		pullTitles []string
		fetcher    PullRequestFetcher
	}{
		{
			name:       "successful",
			pullCount:  2,
			pullTitles: []string{"My Cool PR", "Fix Thing"},
			fetcher:    TestPullRequestFetcher{},
		},
		{
			name:       "error",
			pullCount:  0,
			pullTitles: []string{},
			fetcher:    TestPullRequestFetcher{Error: errors.New("Something went wrong")},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := Pulls(test.fetcher)

			actualPullCount := len(actual)
			if actualPullCount != test.pullCount {
				log.Fatalf(
					"Pulls() %v failed; expected pullCount to be %v but got %v",
					test.name, test.pullCount, actualPullCount)
			}

			actualTitles := make([]string, len(actual))
			for i, pr := range actual {
				actualTitles[i] = *pr.Title
			}

			expectedTitles := make([]string, len(test.pullTitles))
			for i, title := range test.pullTitles {
				expectedTitles[i] = title
			}

			if !reflect.DeepEqual(actualTitles, expectedTitles) {
				log.Fatalf(
					"Pulls() %v failed; expected titles to be %v but got %v",
					test.name, expectedTitles, actualTitles)
			}
		})
	}
}

func TestRepoPulls(t *testing.T) {
}

func TestRepoNames(t *testing.T) {
	tests := []struct {
		name     string
		input    string
		length   int
		expected []string
	}{
		{
			name:     "comma-sep list => array of strings",
			input:    "first,second",
			length:   2,
			expected: []string{"first", "second"},
		},
		{
			name:     "string => array of string",
			input:    "first",
			length:   1,
			expected: []string{"first"},
		},
		{
			name:     "list of empty strings => array of empty string",
			input:    ",,",
			length:   3,
			expected: []string{"", "", ""},
		},
		{
			name:     "empty list => empty array",
			input:    "",
			length:   0,
			expected: []string{},
		},
	}

	for _, test := range tests {
		Config.Github.Repos = test.input

		t.Run(test.name, func(t *testing.T) {
			actual := repoNames()

			if len(actual) != test.length {
				log.Fatalf("repoNames() %v failed; length expected to be %v but got %v", test.name, test.length, len(actual))
			}

			if !reflect.DeepEqual(test.expected, actual) {
				log.Fatalf("repoNames() %v failed; expected %v but got %v", test.name, test.expected, actual)
			}
		})
	}
}

func TestIsPullReviewable(t *testing.T) {
	tests := []struct {
		name     string
		input    *github.PullRequest
		expected bool
	}{
		{
			name:     "draft PR",
			input:    &github.PullRequest{Draft: github.Bool(true)},
			expected: false,
		},
		{
			name:     "ready-for-review PR",
			input:    &github.PullRequest{Draft: github.Bool(false)},
			expected: true,
		},
		{
			name:     "PR default",
			input:    &github.PullRequest{Draft: new(bool)},
			expected: true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := isPullReviewable(test.input)

			if actual != test.expected {
				log.Fatalf("isPullReviewable() %v failed; expected to be %v but got %v", test.name, test.expected, actual)
			}
		})
	}
}

func TestIsPullForTeam(t *testing.T) {
	tests := []struct {
		name     string
		input    *github.PullRequest
		expected bool
	}{
		{
			name:     "no teams",
			input:    &github.PullRequest{},
			expected: false,
		},
		{
			name: "only team",
			input: &github.PullRequest{RequestedTeams: []*github.Team{
				{Slug: github.String("devs")},
			}},
			expected: true,
		},
		{
			name: "not the team",
			input: &github.PullRequest{RequestedTeams: []*github.Team{
				{Slug: github.String("sales")},
			}},
			expected: false,
		},
		{
			name: "one of several teams",
			input: &github.PullRequest{RequestedTeams: []*github.Team{
				{Slug: github.String("sales")},
				{Slug: github.String("devs")},
			}},
			expected: true,
		},
		{
			name: "none of several teams",
			input: &github.PullRequest{RequestedTeams: []*github.Team{
				{Slug: github.String("sales")},
				{Slug: github.String("betatesters")},
			}},
			expected: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			Config.Github.Team = "devs"
			actual := isPullForTeam(test.input)

			if actual != test.expected {
				log.Fatalf("isPullForTeam() %v failed; expected to be %v but got %v", test.name, test.expected, actual)
			}
		})
	}
}

func TestIsPullInGoodTime(t *testing.T) {
	tests := []struct {
		name     string
		input    *github.PullRequest
		expected bool
	}{
		{
			name: "CreatedAt long ago",
			input: &github.PullRequest{
				CreatedAt: &[]time.Time{
					time.Now().Add(time.Hour * -6).Add(time.Minute * -1),
				}[0],
			},
			expected: true,
		},
		{
			name: "CreatedAt within a useful time",
			input: &github.PullRequest{
				CreatedAt: &[]time.Time{
					time.Now().Add(time.Hour * -4),
				}[0],
			},
			expected: true,
		},
		{
			name: "CreatedAt too soon ago",
			input: &github.PullRequest{
				CreatedAt: &[]time.Time{
					time.Now().Add(time.Hour * -1).Add(time.Minute * -59),
				}[0],
			},
			expected: false,
		},
		{
			name: "CreatedAt in the future",
			input: &github.PullRequest{
				CreatedAt: &[]time.Time{
					time.Now().Add(time.Minute * 10),
				}[0],
			},
			expected: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := isPullInGoodTime(test.input)

			if actual != test.expected {
				log.Fatalf("isPullInGoodTime() %v failed; expected to be %v but got %v", test.name, test.expected, actual)
			}
		})
	}
}

func TestIsPullStale(t *testing.T) {
	tests := []struct {
		name     string
		input    *github.PullRequest
		expected bool
	}{
		{
			name: "CreatedAt too long ago",
			input: &github.PullRequest{
				CreatedAt: &[]time.Time{
					time.Now().Add(time.Hour * 24 * -8),
				}[0],
			},
			expected: true,
		},
		{
			name: "CreatedAt within a useful time",
			input: &github.PullRequest{
				CreatedAt: &[]time.Time{
					time.Now().Add(time.Hour * -5),
				}[0],
			},
			expected: false,
		},
		{
			name: "CreatedAt too soon ago",
			input: &github.PullRequest{
				CreatedAt: &[]time.Time{
					time.Now().Add(time.Hour * -2),
				}[0],
			},
			expected: false,
		},
		{
			name: "CreatedAt in the future",
			input: &github.PullRequest{
				CreatedAt: &[]time.Time{
					time.Now().Add(time.Minute * 10),
				}[0],
			},
			expected: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			actual := isPullStale(test.input)

			if actual != test.expected {
				log.Fatalf("isPullStale() %v failed; expected to be %v but got %v", test.name, test.expected, actual)
			}
		})
	}
}

func TestReportStatus(t *testing.T) {
	tests := []struct {
		name      string
		lines     []string
		pulls     []*github.PullRequest
		repoNames []string
		pullCount int
		repoCount int
	}{
		{
			name: "emptyness",
			lines: []string{
				"github/Pulls => Found 0 PRs in 0 repos for devs.",
			},
			pulls:     []*github.PullRequest{},
			pullCount: 0,
			repoNames: []string{},
			repoCount: 0,
		},
		{
			name: "same number",
			lines: []string{
				"github/Pulls => Found 1 PRs in 1 repos for devs.",
				"github/Pulls =>   Pull #5577006791947779410 in my-app by developer: Improvement => https://example.com/",
			},
			pulls: []*github.PullRequest{
				{
					Number:  github.Int(rand.Int()),
					Base:    &github.PullRequestBranch{Repo: &github.Repository{Name: github.String("my-app")}},
					User:    &github.User{Login: github.String("developer")},
					Title:   github.String("Improvement"),
					HTMLURL: github.String("https://example.com/"),
				},
			},
			pullCount: 1,
			repoNames: []string{
				"my-app",
			},
			repoCount: 1,
		},
		{
			name: "different number",
			lines: []string{
				"github/Pulls => Found 0 PRs in 1 repos for devs.",
			},
			pulls:     []*github.PullRequest{},
			pullCount: 0,
			repoNames: []string{
				"my-app",
			},
			repoCount: 1,
		},
	}

	for _, test := range tests {
		Config.Github.Team = "devs"

		t.Run(test.name, func(t *testing.T) {
			actualLines, actualPullCount, actualRepoCount := reportStatus(test.pulls, test.repoNames)

			if !reflect.DeepEqual(actualLines, test.lines) {
				log.Fatalf(
					"reportStatus() %v failed; expected lines to be %v but got %v",
					test.name, test.lines, actualLines)
			}

			if actualPullCount != test.pullCount {
				log.Fatalf(
					"reportStatus() %v failed; expected pullCount to be %v but got %v",
					test.name, test.pullCount, actualPullCount)
			}

			if actualRepoCount != test.repoCount {
				log.Fatalf(
					"reportStatus() %v failed; expected repoCount to be %v but got %v",
					test.name, test.repoCount, actualRepoCount)
			}
		})
	}
}
