package github

import (
	"context"

	"bitbucket.org/sea_wolf/peer/internal/config"
	"github.com/google/go-github/v33/github"
	"golang.org/x/oauth2"
)

var (
	// Config brings in our configuration
	Config config.Config

	// Github connects to the GitHub API for us
	Github *github.Client
)

// Init is to be called before anything
func Init() {
	Config = config.Init()

	Github = github.NewClient(
		oauth2.NewClient(
			context.Background(), oauth2.StaticTokenSource(
				&oauth2.Token{AccessToken: Config.Github.AuthToken},
			),
		),
	)
}
