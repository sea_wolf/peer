package github

import (
	"errors"
	"io/ioutil"
	"log"
	"testing"

	"github.com/jarcoal/httpmock"
)

func TestInit(t *testing.T) {
	t.Run("sets the Config and Github variables from required data", func(t *testing.T) {
		InitTests()
	})
}

func TestAuth(t *testing.T) {
	log.SetOutput(ioutil.Discard)

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	tests := []struct {
		name           string
		githubStatus   int
		githubResponse string
		expected       string
		error          error
	}{
		{
			name:           "When GitHub likes us",
			expected:       "john.doe",
			githubStatus:   200,
			githubResponse: `{"id": 1,"login": "john.doe"}`,
		},
		{
			name:           "When GitHub doesn't like us",
			githubStatus:   403,
			githubResponse: `{"message":"Sorry, don't like you."}`,
			error:          errors.New("GET https://api.github.com/user: 403 Sorry, don't like you. []"),
		},
	}

	for _, tt := range tests {
		InitTests()

		httpmock.RegisterResponder("GET", "https://api.github.com/user",
			httpmock.NewStringResponder(tt.githubStatus, tt.githubResponse))

		t.Run(tt.name, func(t *testing.T) {
			returned, err := Auth()

			if returned != tt.expected {
				t.Errorf("Auth() returned %v but expected %v", returned, tt.expected)
			}
			if err != nil || tt.error != nil {
				if err.Error() != tt.error.Error() {
					t.Errorf("Auth() Error returned `%v` but expected `%v`", err, tt.error)
				}
			}
		})
	}
}
