package github

import "os"

func InitTests() {
	os.Setenv("SLACK_AUTH_TOKEN", "-")
	os.Setenv("SLACK_CHANNEL", "cool-dev-team")

	os.Setenv("GITHUB_AUTH_TOKEN", "-")
	os.Setenv("GITHUB_ORG", "acme-ltd")
	os.Setenv("GITHUB_REPOS", "thing,stuff")
	os.Setenv("GITHUB_TEAM", "devs")

	Init()
}
