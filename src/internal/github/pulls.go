package github

import (
	"context"
	"fmt"
	"log"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/google/go-github/v33/github"
)

// PullRequestFetcher is what the GitHub API endpoint for Pull Request does
type PullRequestFetcher interface {
	List(context.Context, string, string, *github.PullRequestListOptions) ([]*github.PullRequest, *github.Response, error)
}

// Pulls lists the relevent PRs
func Pulls(pullRequestFetcher PullRequestFetcher) []*github.PullRequest {
	repoNames := repoNames()
	pulls := make([]*github.PullRequest, 0)

	wg := sync.WaitGroup{}
	for _, repoName := range repoNames {
		wg.Add(1)

		go func(repoName string) int {
			repoPulls, err := repoPulls(repoName, pullRequestFetcher)
			if err != nil {
				log.Println("github/Pulls => ERROR:", err)

				wg.Done()
				return -1
			}

			for _, repoPull := range repoPulls {
				if isPullReviewable(repoPull) &&
					isPullForTeam(repoPull) &&
					isPullInGoodTime(repoPull) &&
					!isPullStale(repoPull) {
					pulls = append(pulls, repoPull)
				}
			}

			wg.Done()
			return len(repoPulls)
		}(repoName)
	}

	wg.Wait()

	sort.Slice(pulls, func(next, this int) bool {
		// this Before next: sort newest to oldest
		// this After next: sort oldest to newest
		if pulls[this].UpdatedAt != nil && pulls[next].UpdatedAt != nil {
			return pulls[this].UpdatedAt.Before(*pulls[next].UpdatedAt)
		}
		return false
	})

	if Config.Github.Debug {
		output, _, _ := reportStatus(pulls, repoNames)

		for _, line := range output {
			fmt.Println(line)
		}
	}

	return pulls
}

func repoPulls(repoName string, pullRequestFetcher PullRequestFetcher) ([]*github.PullRequest, error) {
	opts := github.PullRequestListOptions{
		State: "OPEN",
	}
	repoPulls, _, err := pullRequestFetcher.List(context.Background(), Config.Github.Org, repoName, &opts)

	if Config.Github.Debug {
		log.Printf("github/Pulls => Obtained %v pulls from %v", len(repoPulls), repoName)
	}

	return repoPulls, err
}

func repoNames() []string {
	repoNames := Config.Github.Repos

	if len(repoNames) > 0 {
		return strings.Split(repoNames, ",")
	}

	return []string{}
}

func isPullReviewable(p *github.PullRequest) bool {
	return !*p.Draft
}

func isPullForTeam(p *github.PullRequest) bool {
	for _, team := range p.RequestedTeams {
		if *team.Slug == Config.Github.Team {
			return true
		}
	}

	return false
}

func isPullInGoodTime(p *github.PullRequest) bool {
	prOpenedAt := p.CreatedAt.UTC()
	cutoffTime := time.Now().UTC().Add(-3 * time.Hour)

	return prOpenedAt.Before(cutoffTime)
}

func isPullStale(p *github.PullRequest) bool {
	prOpenedAt := p.CreatedAt.UTC()
	cutoffTime := time.Now().UTC().Add(-6 * time.Hour)

	return prOpenedAt.Before(cutoffTime)
}

func reportStatus(pulls []*github.PullRequest, repoNames []string) ([]string, int, int) {
	pullCount := len(pulls)
	repoCount := len(repoNames)

	output := []string{}
	output = append(output,
		fmt.Sprintf(
			"github/Pulls => Found %v PRs in %v repos for %v.",
			pullCount, repoCount, Config.Github.Team))

	for _, pull := range pulls {
		output = append(output,
			fmt.Sprintf("github/Pulls =>   Pull #%v in %v by %v: %v => %v",
				*pull.Number,
				*pull.Base.Repo.Name,
				*pull.User.Login,
				*pull.Title,
				*pull.HTMLURL,
			))
	}

	return output, pullCount, repoCount
}
