package github

import (
	"context"
	"log"
)

// Auth ensures we are logged-in successfully
func Auth() (string, error) {
	user, _, err := Github.Users.Get(context.Background(), "")
	if err != nil {
		log.Println("github/Auth => ERROR:", err)
		return "", err
	}

	login := *user.Login
	if Config.Github.Debug {
		log.Println("github/Auth => Login:", login)
	}

	return login, err
}
