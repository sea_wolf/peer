package config

import (
	"log"

	"github.com/Netflix/go-env"
)

// Config is the root node
type Config struct {
	Slack  Slack
	Github Github

	Extras env.EnvSet
}

// Slack is the configs for the Slack API
type Slack struct {
	AuthToken string `env:"SLACK_AUTH_TOKEN,required=true"`
	Channel   string `env:"SLACK_CHANNEL,required=true"`
	Debug     bool   `env:"SLACK_DEBUG,default=false"`
}

// Github is the configs for the GitHub API
type Github struct {
	AuthToken string `env:"GITHUB_AUTH_TOKEN,required=true"`
	Debug     bool   `env:"GITHUB_DEBUG,default=false"`
	Org       string `env:"GITHUB_ORG,required=true"`
	Repos     string `env:"GITHUB_REPOS,required=true"`
	Team      string `env:"GITHUB_TEAM,required=true"`
}

// Init is how to get .env values into the Config struct
func Init() Config {
	var config Config
	extras, err := env.UnmarshalFromEnviron(&config)
	if err != nil {
		log.Fatal(err)
	}

	// Remaining environment variables.
	config.Extras = extras

	return config
}
