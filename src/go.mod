module bitbucket.org/sea_wolf/peer

require (
	github.com/Netflix/go-env v0.0.0-20201224175523-e4d859765ea4
	github.com/google/go-github/v33 v33.0.0
	github.com/jarcoal/httpmock v1.0.8
	github.com/slack-go/slack v0.8.0
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/oauth2 v0.0.0-20180821212333-d2e6202438be
)

go 1.15
