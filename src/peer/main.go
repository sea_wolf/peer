package main

import (
	"log"

	"bitbucket.org/sea_wolf/peer/internal/github"
	"bitbucket.org/sea_wolf/peer/internal/slack"
)

func main() {
	github.Init()
	slack.Init()

	github.Auth()
	slack.Auth()

	messagePullRequests := []slack.PullRequest{}
	for _, pr := range github.Pulls(github.Github.PullRequests) {
		messagePullRequests = append(messagePullRequests, slack.New(pr))
	}

	channelID, timestamp, error := slack.Message{
		PullRequests: messagePullRequests,
	}.Send()

	if error == nil {
		log.Printf("Sent message to %v at %v", channelID, timestamp)
  } else if error.(*slack.NoPullRequests) != nil {
		log.Printf("Message not sent: %v", error.Error())
	} else {
		log.Fatalf("Unable to send message: %v", error)
	}
}
