FROM golang:1.15-alpine AS builder

ENV GO111MODULE=on
ENV CGO_ENABLED=0

RUN apk update && \
    apk add --no-cache ca-certificates=20191127-r5 && \
    update-ca-certificates

WORKDIR /src

COPY src/go.mod .
COPY src/go.sum .
RUN go mod download

WORKDIR /
COPY . .
WORKDIR /src

RUN go fmt ./internal/* && go vet ./internal/* && go test -v ./internal/*  && \
    go fmt ./peer && go vet ./peer && go test -v ./peer  && \
    go build -o /bin/peer ./peer

FROM scratch AS distributable

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /bin/peer /

CMD ["/peer"]